/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"encoding/base64"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"time"

	"gitlab.com/ponci-berlin/ponci/pkg/logging"

	"go.uber.org/zap"

	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"

	"gitlab.com/ponci-berlin/ponci/pkg/credential"

	"github.com/spf13/cobra"
	"gitlab.com/ponci-berlin/ponci/pkg/baercode"
	"gitlab.com/ponci-berlin/ponci/pkg/qr"
)

const (
	defaultFirstName                = "Max"
	defaultLastName                 = "Mustermann"
	defaultProcedure                = 1
	defaultProcedureTimeOffsetHours = 0
	defaultOperator                 = "Testing GmbH"
	defaultOutputDir                = "."
	defaultResult                   = false

	aesKeyFileName = "aeskey.hex"
	b64FileName    = "baercode.b64"
	qrFileName     = "baercode.png"
	bundleFileName = "cert_bundle.cose"

	dayLength = time.Hour * 24
)

var (
	firstName                string
	lastName                 string
	procedure                int
	procedureTimeOffsetHours int
	operator                 string
	result                   bool
	outputDir                string
	force                    bool

	cmdTestGen = &cobra.Command{
		Use:   "testgen",
		Short: "Generates files to test with",
		Long:  `This generates a certificate, an AES key, a QR code and the base64 encoded in it.'`,
		RunE: func(cmd *cobra.Command, args []string) error {
			aesKeyPath, b64Path, qrPath, bundlePath, err := getPaths(outputDir, force)
			if err != nil {
				return err
			}

			dob := time.Date(1990, time.April, 1, 00, 00, 00, 00, time.UTC)
			procedureTime := time.Date(2021, time.May, 1, 8, 0, 0, 0, time.UTC)
			procedureTime.Add(time.Hour * time.Duration(procedureTimeOffsetHours))

			procedures := []credential.Procedure{
				{
					Type: credential.ProcedureTypePCR,
					Time: procedureTime,
				},
			}

			cred1 := credential.New(firstName, lastName, dob, procedures, operator, result)
			cred, err := cred1.ToCBOR()
			if err != nil {
				panic(err)
			}
			err = writeFile(cred, path.Join(outputDir, "credential.cbor"))
			if err != nil {
				panic(err)
			}

			kmsKeyID, kmsCertKey, _, err := keymanagement.RollKeys(
				logger.With(zap.String(logging.SubComponentField, "key roller")),
				true,
				outputDir, "kms-",
			)
			if err != nil {
				return fmt.Errorf("couldn't generate keys: %w", err)
			}

			keyID, certKey, aesKey, err := keymanagement.RollKeys(
				logger.With(zap.String(logging.SubComponentField, "key roller")),
				true,
				outputDir, "baercode-",
			)
			if err != nil {
				return err
			}
			codeRaw, err := baercode.CreateBaerCode(
				firstName,
				lastName,
				dob,
				procedures,
				operator,
				result,
				keyID,
				certKey,
				aesKey,
			)
			if err != nil {
				return err
			}

			keySet := keymanagement.KeySet{
				CredType:  credential.CredTypeNegativeTest,
				AESKey:    aesKey,
				ECCKey:    &certKey.PublicKey,
				ValidFrom: time.Now().UTC(),
				ValidTo:   time.Now().UTC().Add(dayLength),
			}
			signedBundle, err := keymanagement.GenerateSignedBundle(
				logger,
				[]keymanagement.KeySet{keySet},
				kmsCertKey,
				kmsKeyID,
			)
			if err != nil {
				return err
			}

			b64 := base64.StdEncoding.EncodeToString(codeRaw)
			qrcode, err := qr.Create(b64, baercode.DefaultQRScale)
			if err != nil {
				return fmt.Errorf("couldn't create QR code: %w", err)
			}
			_, version, err := baercode.VerifyBaerCode(b64, &certKey.PublicKey, aesKey)

			if err != nil {
				return fmt.Errorf("not a valid ponc: %w", err)
			}

			logger.Info("BärCODE version", zap.Uint16("version", version))

			// The cert writers don't return errors, so this function doesn't either.
			// They will bail if anything goes wrong, we probably should clean that up soon.
			// writeCertificateFiles(certKey, cert, certPath, certKeyPath)

			if err = keymanagement.WriteHexFile(aesKey, aesKeyPath); err != nil {
				return err
			}

			if err = writeFile([]byte(b64), b64Path); err != nil {
				return err
			}

			if err = writeFile(qrcode, qrPath); err != nil {
				return err
			}

			return writeFile(signedBundle, bundlePath)
		},
	}
)

//nolint:gochecknoinits
func init() {
	cmdTestGen.PersistentFlags().StringVarP(
		&outputDir,
		"output-dir",
		"d",
		defaultOutputDir,
		fmt.Sprintf("Output directory (default: %s)", defaultOperator),
	)
	cmdTestGen.PersistentFlags().StringVarP(
		&firstName,
		"first-name",
		"n",
		defaultFirstName,
		fmt.Sprintf("First name in the Credential (default: %s)", defaultFirstName),
	)
	cmdTestGen.PersistentFlags().StringVarP(
		&lastName,
		"last-name",
		"l",
		defaultLastName,
		fmt.Sprintf("Name in the Credential (default: %s)", defaultLastName),
	)
	cmdTestGen.PersistentFlags().StringVarP(
		&operator,
		"operator",
		"o",
		defaultOperator,
		fmt.Sprintf("Name of the operator (default: %s)", defaultOperator),
	)
	cmdTestGen.PersistentFlags().IntVarP(
		&procedure,
		"procedure",
		"p",
		defaultProcedure,
		fmt.Sprintf("Procedure number (default: %d)", defaultProcedure),
	)
	cmdTestGen.PersistentFlags().IntVarP(
		&procedureTimeOffsetHours,
		"proceduretime-offset",
		"t",
		defaultProcedureTimeOffsetHours,
		fmt.Sprintf("Procedure time offset from now (in hours) (default: %d)", defaultProcedureTimeOffsetHours),
	)
	cmdTestGen.PersistentFlags().BoolVarP(
		&result,
		"result",
		"r",
		defaultResult,
		"Generate a positive result for the test (default: false)",
	)
	cmdTestGen.PersistentFlags().BoolVarP(
		&force,
		"force",
		"f",
		false,
		"Overwrite already existing files (default: false)",
	)
}

func getPaths(dir string, overwrite bool) (aesKeyPath, b64Path, qrPath, bundlePath string, err error) {
	absPath, err := filepath.Abs(dir)
	if err != nil {
		return
	}

	o, err := os.Stat(absPath)
	if !os.IsNotExist(err) && !o.IsDir() {
		return
	}

	if os.IsNotExist(err) {
		err = os.Mkdir(absPath, 0700)
		if err != nil {
			return
		}
	}

	aesKeyPath = path.Join(absPath, aesKeyFileName)
	b64Path = path.Join(absPath, b64FileName)
	qrPath = path.Join(absPath, qrFileName)
	bundlePath = path.Join(absPath, bundleFileName)

	if !overwrite {
		for _, p := range []string{aesKeyPath, b64Path, qrPath, bundlePath} {
			if _, statErr := os.Stat(p); !os.IsNotExist(statErr) {
				err = fmt.Errorf("file %s exists, refusing to overwrite", p)
				return
			}
		}
	}

	return
}

func writeFile(contents []byte, filePath string) (err error) {
	f, err := os.Create(filePath)
	if err != nil {
		return
	}
	defer f.Close()

	n, err := f.Write(contents)
	if err != nil {
		return
	}

	if n != len(contents) {
		return fmt.Errorf("wrote %d bytes to %s, but expected %d bytes", n, filePath, len(contents))
	}

	return nil
}
