/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"net/http"
	"time"

	ginprometheus "github.com/zsais/go-gin-prometheus"
	"gitlab.com/ponci-berlin/ponci/pkg/metrics"

	ginzap "github.com/gin-contrib/zap"
	"go.uber.org/zap"

	"gitlab.com/ponci-berlin/ponci/pkg/logging"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ponci-berlin/ponci/pkg/issuer"
)

type issuerConfig struct {
	WebServer webConfig      `mapstructure:"webServer"`
	Issuer    issuer.Config  `mapstructure:"issuer"`
	Metrics   metrics.Config `mapstructure:"metrics"`
}

var cmdIssuer = &cobra.Command{
	Use:   "issuer",
	Short: "Web service for collecting test data and issuing PoNCs",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		config := issuerConfig{}
		err := viper.Unmarshal(&config)
		if err != nil {
			logger.Fatal("reading config file: " + err.Error())
		}

		metrics.Start(logger.With(zap.String(logging.ComponentField, "metrics")), config.Metrics)

		issuer := issuer.New(
			logger.With(zap.String(logging.ComponentField, "issuer_setup")),
			config.Issuer,
		)
		router := gin.New()

		router.Use(logging.Injector(logger.With(zap.String(logging.ComponentField, "application"))))
		router.Use(ginzap.Ginzap(
			logger.With(zap.String(logging.ComponentField, "request_log")),
			time.RFC3339,
			true,
		))
		router.Use(ginzap.RecoveryWithZap(
			logger.With(zap.String(logging.ComponentField, "recovery_log")),
			true,
		))

		promMiddleware := ginprometheus.NewPrometheus("issuer")
		promMiddleware.Use(router)

		router.POST("/createCredential", issuer.HandleCreateCredential)
		router.GET("/healthz", issuer.ServiceLiveness)
		router.GET("/", issuer.HandleGet)
		router.StaticFS("/public", http.Dir("public"))

		if config.WebServer.ListenSocket == "" {
			config.WebServer.ListenSocket = "localhost:8080"
		}

		logger.Fatal("server exited", zap.Error(http.ListenAndServe(config.WebServer.ListenSocket, router)))
	},
}
