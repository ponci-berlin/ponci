/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"gitlab.com/ponci-berlin/ponci/pkg/logging"

	"go.uber.org/zap"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func main() {
	if err := rootCmd.Execute(); err != nil {
		logger.Fatal("exiting", zap.Error(err))
	}
}

var (
	logger *zap.Logger

	cfgFile string
	rootCmd = &cobra.Command{
		Use:   "ponc",
		Short: "ponc is a tool for generating verifiable credentials",
		Long:  ``,
	}
)

//nolint:gochecknoinits
func init() {
	logger = logging.New()
	cobra.OnInitialize(initConfig)
	viper.AutomaticEnv()

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.cobra.yaml)")
	rootCmd.PersistentFlags().Bool("viper", true, "use Viper for configuration")
	err := viper.BindPFlag("useViper", rootCmd.PersistentFlags().Lookup("viper"))
	if err != nil {
		logger.Fatal("coudln't bind viper flag", zap.Error(err))
	}
	setConfigDefaults()

	rootCmd.AddCommand(cmdIssuer)
	rootCmd.AddCommand(cmdKeyServer)
	rootCmd.AddCommand(cmdTestGen)
	rootCmd.AddCommand(cmdMigrate)
	rootCmd.AddCommand(cmdCreateUser)
	rootCmd.AddCommand(cmdVerify)
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		viper.SetConfigName("ponc.config")
	}

	viper.AutomaticEnv()
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err == nil {
		logger.Info("running with configuration", zap.String("config_file", viper.ConfigFileUsed()))
	}
}
