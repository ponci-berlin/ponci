/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package issuer

import (
	"crypto/ecdsa"
	"errors"
	"os"
	"sync"
	"time"

	"go.uber.org/zap"

	"github.com/fxamacker/cbor/v2"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

type ProcedureType int

type Issuer struct {
	CredentialType    credential.Type `cbor:"-"`
	LastUpdated       time.Time
	currentKey        **KeySet `cbor:"-"`
	Keys              []*KeySet
	generatingKeys    sync.RWMutex `cbor:"-"`
	runningKeyManager sync.Mutex   `cbor:"-"`
	Config            `cbor:"-"`
	// parsed from config no start and result cached so we don't need to deal with parsing errors during operation
	keyRotationTime   time.Time `cbor:"-"`
	KeyRotationPeriod time.Duration
	Logger            *zap.Logger `cbor:"-"`
}

const keyRotationTimeFormat = "15:04"

type Config struct {
	CredentialTypeConfig string `mapstructure:"credentialType"`
	UserID               string `mapstructure:"userID"`
	Password             string `mapstructure:"password"`
	OTPSecret            string `mapstructure:"otpSecret"`
	Debug                bool   `mapstructure:"debug"`
	DebugNoKeySubmit     bool   `mapstructure:"debugnokeysubmit"`
	// SaveFilePath sets the path of the file that should be used for the state-save file
	SaveFilePath string `mapstructure:"saveFilePath"`
	// WriteDebugFiles sets to write debug files of the certs and keys.
	WriteDebugFiles bool `mapstructure:"writeDebugFiles"`
	// DebugFilePrefix is the prefix used to name the debug files.
	DebugFilePrefix string `mapstructure:"debugFilePrefix"`
	// DebugFilePath is the path used to store the debug files.
	DebugFilePath string `mapstructure:"debugFilePath"`
	// CAURL is the url where the certificate authority can be found. This is also called the key management server.
	CAURL string `mapstructure:"caURL"`
	// OrgName is the organisation name of the organisation to use for certificate generation
	OrgName           string `mapstructure:"orgName"`
	EncryptionKeyName string `mapstructure:"encryptionKeyName"`
	// KeyRotationTime configures the approximate time during the day that new keys should be submitted in HH:mm
	KeyRotationTime string `mapstructure:"keyRotationTime"`
	// KeyUpdatePollPeriod configures how often in minutes this service should check
	// to see if new keys should be used or created.
	KeyUpdatePollPeriod int64 `mapstructcure:"keyUpdatePollPeriod"`
}

func New(logger *zap.Logger, config Config) *Issuer {
	data, err := ReadHexFile(config.SaveFilePath)
	logger = logger.With(zap.String("path", config.SaveFilePath))
	var is = &Issuer{
		Logger: logger,
	}
	switch {
	case err == nil:
		err := cbor.Unmarshal(data, is)
		if err != nil {
			logger.Fatal(
				"save file corrupted",
				zap.Error(err))
		}
	case errors.Is(err, os.ErrNotExist):
		// we don't need to do anything here, just ignore the error and set up fresh
		logger.Debug("empty file, creating new one")

	default:
		logger.Fatal(
			"unknown error occurred",
			zap.Error(err),
		)
		return nil // this is present to make IDEs and linters happy, the previous line calls exit()
	}
	is.Config = config

	is.CredentialType, is.KeyRotationPeriod, err = setTypeAndRotation(config.CredentialTypeConfig)
	if err != nil {
		logger.Fatal("couldn't set credential type and rotation period", zap.Error(err))
		return nil
	}

	if is.KeyUpdatePollPeriod < 1 {
		is.KeyUpdatePollPeriod = 1
	}

	is.keyRotationTime, err = time.Parse(keyRotationTimeFormat, config.KeyRotationTime) // parsing checked on start
	if err != nil {
		logger.Fatal(
			"unable to parse keyrotation time",
			zap.String("key_rotation_config", config.KeyRotationTime),
			zap.Error(err),
		)
		return nil // this is present to make IDEs and linters happy, the previous line calls exit()
	}

	err = is.Save()
	if err != nil {
		logger.Fatal("unable to save state file", zap.Error(err))
	}

	go is.startKeyManagementService()

	return is
}

func (is *Issuer) Save() error {
	is.LastUpdated = time.Now()
	is.generatingKeys.RLock()
	cborBytes, err := cbor.Marshal(is)
	is.generatingKeys.RUnlock()
	if err != nil {
		return err
	}
	err = WriteHexFile(cborBytes, is.SaveFilePath)
	return err // TODO
}

func NewKeyID(pub *ecdsa.PublicKey) []byte {
	num := pub.X.Bytes()
	return num[len(num)-16:]
}

func (is *Issuer) GetCurrentKey() *KeySet {
	if is.currentKey == nil {
		return nil
	}
	is.generatingKeys.RLock()
	defer is.generatingKeys.RUnlock()
	key := *is.currentKey
	return key
}

func setTypeAndRotation(credTypeConfig string) (credType credential.Type, period time.Duration, err error) {
	switch credTypeConfig {
	case "vaccine":
		credType = credential.CredTypeVaccine
		period = vaccineKeyRotationPeriod
	case "test":
		credType = credential.CredTypeNegativeTest
		period = testKeyRotationPeriod
	case "unspecified":
		credType = credential.CredTypeUnspecified
		period = dayLength
	default:
		err = errors.New(
			"unrecognised credential type specified in issuer.credentialType. Please use either 'vaccine' or 'test'",
		)
		return 0, 0, err
	}

	return credType, period, nil
}
