/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package issuer

import (
	"encoding/hex"
	"io/ioutil"
	"os"
)

func WriteHexFile(key []byte, target string) error {
	keyString := hex.EncodeToString(key)
	return ioutil.WriteFile(target, []byte(keyString), os.ModePerm) // TODO handle err
}

func ReadHexFile(target string) ([]byte, error) {
	hexData, err := ioutil.ReadFile(target) // TODO handle err
	if err != nil {
		return nil, err
	}
	buf := make([]byte, hex.DecodedLen(len(hexData)))
	_, err = hex.Decode(buf, hexData)
	return buf, err
}
