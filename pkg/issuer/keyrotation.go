/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package issuer

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"gitlab.com/ponci-berlin/ponci/pkg/logging"

	"go.uber.org/zap"

	"github.com/cenkalti/backoff/v4"
	"github.com/fxamacker/cbor/v2"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"

	"gitlab.com/ponci-berlin/ponci/pkg/authentication"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

const dayLength = time.Hour * 24
const delayToValid = dayLength
const testKeyValidDuration = dayLength * 3
const vaccineKeyValidDuration = dayLength * 365

const testKeyRotationPeriod = dayLength
const vaccineKeyRotationPeriod = dayLength * 365

var (
	keysSubmitted = promauto.NewCounter(prometheus.CounterOpts{
		Name: "issuer_keys_submitted_total",
		Help: "The total number of keys submitted.",
	})
	submitErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "issuer_key_submission_errors_total",
		Help: "The total number of errors encountered while submitting keys.",
	})
)

func (is *Issuer) SubmitNewKeys(set *KeySet) error {
	var id []byte

	logger := is.Logger.With(zap.String(logging.SubComponentField, "SubmitNewKeys"))

	csrRequest := func() error {
		csrLogger := logger.With(zap.String("CAURL", is.CAURL))
		csrLogger.Info("submitting new keys")
		csr, err := is.MarshalSubmitKeysRequest(set)
		if err != nil {
			return fmt.Errorf("marshalling key submission: %w", err)
		}

		req, err := http.NewRequest("POST", is.CAURL, bytes.NewBuffer(csr))
		if err != nil {
			return fmt.Errorf("creating key submission request: %w", err)
		}

		req.Header["Content-Type"] = []string{"application/octet-stream"}

		otpPassword, err := authentication.GenerateOTPPassword(is.OTPSecret)
		if err != nil {
			return fmt.Errorf("generating OTP for CSR authentication: %w", err)
		}
		authString := fmt.Sprintf("%s:%s:%s", is.UserID, is.Password, otpPassword)
		req.Header["Authorization"] = []string{authString}

		csrLogger.Info("submitting CSR", zap.Int("csr_bytes", len(csr)))
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return fmt.Errorf("submitting CSR to key management server: %w", err)
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			return fmt.Errorf("server error when submitting csr: %d", resp.StatusCode)
		}
		keysSubmitted.Inc()

		id, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("reading response body: %w", err)
		}

		return nil
	}

	err := backoff.RetryNotify(csrRequest, backoff.NewExponentialBackOff(), func(err error, duration time.Duration) {
		submitErrors.Inc()
		logger.Error("couldn't submit csr", zap.Error(err))
	})
	if err != nil {
		return err
	}

	if len(id) > 0 {
		set.Id = id
	}
	return nil
}

func (is *Issuer) startKeyManagementService() {
	is.runningKeyManager.Lock()
	defer is.runningKeyManager.Unlock()

	logger := is.Logger.With(zap.Int("credential_type", int(is.CredentialType)))

	for {
		if nextKey := shouldUseNextKey(&is.generatingKeys, is.GetCurrentKey(), is.Keys); nextKey != nil {
			is.generatingKeys.Lock()
			is.currentKey = nextKey
			is.generatingKeys.Unlock()
			continue
		}

		//nolint:nestif
		if shouldSubmitNewKey(&is.generatingKeys, is.keyRotationTime, is.Keys, is.KeyRotationPeriod) {
			newKey, err := NewKeySet(is.CredentialType)
			if err != nil {
				logger.Error("couldn't generate keyset", zap.Error(err))
				// TODO handle err and maybe retry immediately?
				continue
			}
			err = is.SubmitNewKeys(newKey)
			if err != nil {
				logger.Error("couldn't submit keyset", zap.Error(err))
				// TODO handle err and maybe retry immediately?
				continue
			}

			// set key validity times
			now := time.Now()
			newKey.ValidFrom = now
			if len(is.Keys) > 0 && is.GetCurrentKey().ValidTo.After(now.Add(delayToValid)) {
				newKey.ValidFrom = time.Now().Add(delayToValid)
			}
			if newKey.CredType == credential.CredTypeVaccine {
				newKey.ValidTo = newKey.ValidFrom.Add(vaccineKeyValidDuration)
			} else {
				newKey.ValidTo = newKey.ValidFrom.Add(testKeyValidDuration)
			}

			is.generatingKeys.Lock()
			is.Keys = append(is.Keys, newKey)
			is.generatingKeys.Unlock()

			err = is.Save()
			if err != nil {
				logger.Error("couldn't save state", zap.Error(err))
			}
			continue
		}

		<-time.After(time.Minute * time.Duration(is.Config.KeyUpdatePollPeriod))
	}
}

func shouldSubmitNewKey(mut *sync.RWMutex, submitTime time.Time, keys []*KeySet, keyRotationPeriod time.Duration) bool {
	if len(keys) == 0 {
		return true
	}
	now := time.Now()
	mut.RLock()
	newest := keys[len(keys)-1]
	mut.RUnlock()

	if newest.ValidTo.Before(now) {
		return true
	}

	if now.Before(
		newest.ValidFrom.Add(keyRotationPeriod - delayToValid - time.Hour),
	) {
		return false
	}

	y, m, d := now.Date()
	todaySubmitTime := time.Date(y, m, d, submitTime.Hour(), submitTime.Minute(), 0, 0, now.Location())

	return now.After(todaySubmitTime)
}

func shouldUseNextKey(mut *sync.RWMutex, currentKey *KeySet, keys []*KeySet) **KeySet {
	if len(keys) == 0 {
		return nil
	}

	mut.RLock()
	defer mut.RUnlock()

	now := time.Now()
	// find the newest key that is valid
	for i := len(keys) - 1; i > 0; i-- {
		if currentKey == nil {
			return &keys[i]
		}
		if keys[i].ValidFrom.Before(now) {
			if bytes.Equal(keys[i].Id, currentKey.Id) {
				return nil
			}
			return &keys[i]
		}
	}

	if currentKey == nil {
		// return the first key for use immediately for when we onboard labs.
		// This may cause some issues, but we don't have better options at the moment.
		return &keys[0]
	}
	return nil
}

type SubmitKeysRequest struct {
	_        struct{} `cbor:"toarray"`
	CredType credential.Type
	AesKey   cbor.RawMessage
	ECCKey   cbor.RawMessage
}

func (is *Issuer) MarshalSubmitKeysRequest(set *KeySet) ([]byte, error) {
	aesBytes, err := cose.MarshallAESKey(set.AESKey)
	if err != nil {
		return nil, fmt.Errorf("marshalling encryption key for submission: %w", err)
	}

	eccBytes, err := cose.MarshallECKey(&set.ECCKey.PublicKey)
	if err != nil {
		return nil, fmt.Errorf("marshalling ecc key for submission: %w", err)
	}
	req := SubmitKeysRequest{
		CredType: set.CredType,
		AesKey:   aesBytes,
		ECCKey:   eccBytes,
	}

	reqBytes, err := cbor.Marshal(req)
	if err != nil {
		return nil, fmt.Errorf("marshalling submitKeysRequest: %w", err)
	}

	return reqBytes, nil
}
