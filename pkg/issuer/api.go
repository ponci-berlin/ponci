/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package issuer

import (
	"encoding/base64"
	"html"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"go.uber.org/zap"

	"github.com/gin-gonic/gin"
	"gitlab.com/ponci-berlin/ponci/pkg/baercode"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

var (
	baercodesIssued = promauto.NewCounter(prometheus.CounterOpts{
		Name: "issuer_baercodes_issued_total",
		Help: "The total number of baercodes issuer.",
	})
	issuerErrs = promauto.NewCounter(prometheus.CounterOpts{
		Name: "issuer_baercode_issuer_errors_total",
		Help: "The total number of errors while issuing baercodes.",
	})
)

type createRequest struct {
	FirstName         string
	LastName          string
	DateOfBirth       time.Time              // RFC3339 date in UTC timezone
	Procedures        []credential.Procedure `json:"Procedures"`
	ProcedureOperator string
}

func (is *Issuer) HandleGet(ctx *gin.Context) {
	http.ServeFile(ctx.Writer, ctx.Request, "public/index-issuer.html") // TODO configurable
}

func (is *Issuer) HandleCreateCredential(ctx *gin.Context) {
	req := createRequest{}
	err := ctx.BindJSON(&req)
	if err != nil {
		ctxErr := ctx.Error(err)
		if ctxErr != nil {
			is.Logger.Error(
				"error during credential creation and pushing it to the context",
				zap.Error(err),
				zap.Error(ctxErr),
			)
		}
		return
	}

	req.FirstName = html.EscapeString(req.FirstName)
	req.LastName = html.EscapeString(req.LastName)
	req.ProcedureOperator = html.EscapeString(req.ProcedureOperator)

	// Cormirnaty is our first vaccine, everything after that should be a vaccine (for now).
	procedureResult := req.Procedures[0].Type >= credential.ProcedureTypeCormirnaty

	baercode := is.Issue(req.FirstName, req.LastName, req.DateOfBirth, req.Procedures, procedureResult,
		req.ProcedureOperator)
	b64bc := base64.StdEncoding.EncodeToString(baercode)
	baercodesIssued.Inc()
	ctx.String(http.StatusOK, b64bc)
}

// Issue generates a new BärCODE as a bitmap and returns it
// DoB will contain only date information with everything else being 0'd (including TZ, so that it uses UTC)
// This could potentially take a credential object containing the same fields.
func (is *Issuer) Issue(
	firstName string,
	lastName string,
	dob time.Time,
	procedures []credential.Procedure,
	result bool,
	orgName string,
) []byte {
	keySet := is.GetCurrentKey()

	if orgName == "" {
		orgName = is.OrgName
	}

	is.Logger.Debug(
		"issuing BärCODE",
		zap.Binary("key_id", keySet.Id),
		zap.String("org_name", orgName),
	)

	data, err := baercode.CreateBaerCodeQR(
		firstName,
		lastName,
		dob,
		procedures,
		orgName,
		result,
		keySet.Id,
		keySet.ECCKey.PrivateKey,
		keySet.AESKey,
	)
	if err != nil {
		issuerErrs.Inc()
		// TODO: Handle error better
		is.Logger.Fatal("failing to issue", zap.Error(err))
	}
	return data
}

// ServiceLiveness checks the status of the service and returns a 200 if the service is in a good state, and a 500 if
// not.
func (is *Issuer) ServiceLiveness(ctx *gin.Context) {
	now := time.Now().UTC()
	nextPeriod := now.Add(is.KeyRotationPeriod)
	haveKeyForNextPeriod := false
	currentKey := *is.currentKey

	for _, ks := range is.Keys {
		if ks.ValidFrom.Before(nextPeriod) && ks.ValidTo.After(nextPeriod) {
			haveKeyForNextPeriod = true
			break
		}
	}

	if currentKey.ValidFrom.After(now) {
		status := http.StatusInternalServerError
		// If we have a key for the next period, we're in the bootstrapping period and things are OK.
		if haveKeyForNextPeriod {
			status = http.StatusOK
			ctx.String(status, "current key too new, but have valid future key, bootstrapping phase active")
			return
		}
		ctx.String(status, "current key too new and no valid key for future period.")
		return
	}

	if currentKey.ValidTo.Before(now) {
		ctx.String(http.StatusInternalServerError, "current key expired")
		return
	}

	if !haveKeyForNextPeriod {
		ctx.String(http.StatusOK, "current key valid but no future key available")
		return
	}

	ctx.String(http.StatusOK, "OK")
}
