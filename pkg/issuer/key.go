/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package issuer

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"fmt"

	"github.com/fxamacker/cbor/v2"
)

type privateKey struct {
	*ecdsa.PrivateKey
}

func (pk *privateKey) MarshalCBOR() ([]byte, error) {
	der, err := x509.MarshalECPrivateKey(pk.PrivateKey)
	if err != nil {
		return nil, err
	}
	return cbor.Marshal(der)
}

func (pk *privateKey) UnmarshalCBOR(data []byte) error {
	der := []byte{}
	err := cbor.Unmarshal(data, &der)
	if err != nil {
		return err
	}
	ecKey, err := x509.ParseECPrivateKey(der)
	if err != nil {
		return err
	}
	pk.PrivateKey = ecKey
	return nil
}

func (pk *privateKey) getID() []byte {
	x := pk.X.Bytes()
	return x[len(x)-16:]
}

func generateSignatureKeys() (*privateKey, error) {
	priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		return nil, err
	}
	return &privateKey{priv}, err
}

func generateEncryptionKey() ([]byte, error) {
	key := make([]byte, 16)
	var err error
	for i := 0; i < 5; i++ {
		_, err = rand.Read(key)
		if err == nil {
			break
		}
	}
	if err != nil {
		return nil, fmt.Errorf("reading random source: %w", err)
	}
	return key, nil
}
