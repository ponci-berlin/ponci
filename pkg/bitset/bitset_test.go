package bitset_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ponci-berlin/ponci/pkg/bitset"
)

//nolint:funlen
func TestSet(t *testing.T) {
	type args struct {
		b     bitset.Bits
		flags []uint
	}
	tests := []struct {
		name string
		args args
		want bitset.Bits
	}{
		{
			"Test first bit set.",
			args{
				bitset.Bits(0),
				[]uint{
					0,
				},
			},
			bitset.Bits(1),
		},
		{
			"Test second and third bits set.",
			args{
				bitset.Bits(0),
				[]uint{
					1,
					2,
				},
			},
			bitset.Bits(6),
		},
		{
			"Test only second bit set.",
			args{
				bitset.Bits(0),
				[]uint{
					2,
				},
			},
			bitset.Bits(4),
		},
		{
			"Test only third bit set.",
			args{
				bitset.Bits(0),
				[]uint{
					3,
				},
			},
			bitset.Bits(8),
		},
		{
			"Test first and third bit set.",
			args{
				bitset.Bits(0),
				[]uint{
					1,
					3,
				},
			},
			bitset.Bits(10),
		},
		{
			"Test adding third bit to first.",
			args{
				bitset.Bits(1),
				[]uint{
					3,
				},
			},
			bitset.Bits(9),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := tt.args.b
			for _, flag := range tt.args.flags {
				b = bitset.Set(b, flag)
			}
			assert.Equal(t, tt.want, b)
		})
	}
}

func TestClear(t *testing.T) {
	type args struct {
		b    bitset.Bits
		flag uint
	}
	tests := []struct {
		name string
		args args
		want bitset.Bits
	}{
		{
			"Clear only bit",
			args{
				bitset.Bits(1),
				0,
			},
			bitset.Bits(0),
		},
		{
			"Clear last bit, of two set",
			args{
				bitset.Bits(5),
				2,
			},
			bitset.Bits(1),
		},
		{
			"Clear middle bit, of first three set",
			args{
				bitset.Bits(7),
				1,
			},
			bitset.Bits(5),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, bitset.Clear(tt.args.b, tt.args.flag))
		})
	}
}

//nolint:funlen
func TestHas(t *testing.T) {
	type args struct {
		b     bitset.Bits
		flags []uint
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Has five bits set",
			args{
				bitset.Bits(31),
				[]uint{
					0,
					1,
					2,
					3,
					4,
				},
			},
			true,
		},
		{
			"Has fourth and third bit set",
			args{
				bitset.Bits(12),
				[]uint{
					2,
					3,
				},
			},
			true,
		},
		{
			"Has only fourth bit set",
			args{
				bitset.Bits(8),
				[]uint{
					3,
				},
			},
			true,
		},
		{
			"Doesn't have third bit set.",
			args{
				bitset.Bits(23),
				[]uint{
					3,
				},
			},
			false,
		},
		{
			"Doesn't have first five bits set.",
			args{
				bitset.Bits(64),
				[]uint{
					0,
					1,
					2,
					3,
					4,
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, flag := range tt.args.flags {
				assert.Equal(t, tt.want, bitset.Has(tt.args.b, flag))
			}
		})
	}
}

//nolint:funlen
func TestToggle(t *testing.T) {
	type args struct {
		b    bitset.Bits
		flag uint
	}
	tests := []struct {
		name string
		args args
		want bitset.Bits
	}{
		{
			"Toggle first bit off",
			args{
				bitset.Bits(7),
				0,
			},
			bitset.Bits(6),
		},
		{
			"Toggle middle bit off",
			args{
				bitset.Bits(7),
				1,
			},
			bitset.Bits(5),
		},
		{
			"Toggle only bit off",
			args{
				bitset.Bits(8),
				3,
			},
			bitset.Bits(0),
		},
		{
			"Toggle first bit on",
			args{
				bitset.Bits(6),
				0,
			},
			bitset.Bits(7),
		},
		{
			"Toggle middle bit on",
			args{
				bitset.Bits(5),
				1,
			},
			bitset.Bits(7),
		},
		{
			"Toggle only bit on",
			args{
				bitset.Bits(0),
				3,
			},
			bitset.Bits(8),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, bitset.Toggle(tt.args.b, tt.args.flag))
		})
	}
}
