/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package user

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"gitlab.com/ponci-berlin/ponci/pkg/bitset"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"

	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"

	"gitlab.com/ponci-berlin/ponci/pkg/authentication"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	UserID       string `gorm:"uniqueIndex"`
	Password     string // formatted with hash, see storageFormat in passwords.go
	OTPSecret    string
	OrgID        string
	ProcedureSet int
}

func New(dbHandle *gorm.DB, orgID string, allowedProcedureTypes []credential.Type) (*User, error) {
	id := base64.StdEncoding.EncodeToString(keymanagement.NewRandomString())
	password := base64.StdEncoding.EncodeToString(keymanagement.NewRandomString())
	passhash, err := authentication.GenerateHash(authentication.DefaultPasswordConfig, []byte(password), rand.Reader)
	if err != nil {
		return nil, fmt.Errorf("couldn't generate pass hash: %w", err)
	}
	otpSecret, _ := authentication.NewOTP()
	procedureSet := bitset.Bits(0)
	for _, procedureType := range allowedProcedureTypes {
		procedureSet = bitset.Set(procedureSet, uint(procedureType))
	}
	user := &User{
		UserID:       id,
		Password:     passhash,
		OTPSecret:    otpSecret,
		OrgID:        orgID,
		ProcedureSet: int(procedureSet),
	}

	if err := dbHandle.Create(user).Error; err != nil {
		return nil, err
	}

	// Quickly set the password to the unhashed one for usability.
	// TODO: Do this in a cleaner way.
	user.Password = password
	return user, nil
}

func (u *User) Save(dbHandle *gorm.DB) error {
	return dbHandle.Save(u).Error
}

func (u *User) ProcedureTypeAllowed(procedureType credential.Type) bool {
	return bitset.Has(bitset.Bits(u.ProcedureSet), uint(procedureType))
}

// Fetch retrieves the user with the given UserID
// The returned user is not valid if the error is not nil.
func Fetch(dbHandle *gorm.DB, userID string) (*User, error) {
	user := &User{}
	result := dbHandle.Where("user_id = ?", userID).First(user)
	return user, result.Error
}
