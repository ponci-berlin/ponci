/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keymanagement

import (
	"crypto/ecdsa"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"go.uber.org/zap"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"

	"github.com/fxamacker/cbor/v2"
)

var (
	bundleCount = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "kms_bundle_count",
		Help: "The amount of keys in the bundle.",
	})
)

type Bundle struct { // this should be wrapped in a cose message that is signed using of our root keys
	Version int16
	Date    time.Time // utc unix timestamp
	Keys    []cborKeySet
}

// GenerateBundle generates an unsigned bundle.
func GenerateBundle(logger *zap.Logger, keySets []KeySet) (cborBundle []byte, err error) {
	cborKeySets := make([]cborKeySet, 0)
	for _, ks := range keySets {
		cborKeySets = append(cborKeySets, ks.toCBORKeySet())
	}
	bundle := Bundle{
		Version: 1,
		Date:    time.Now().UTC(),
		Keys:    cborKeySets,
	}

	bundleCount.Set(float64(len(bundle.Keys)))
	logger.Info("Generated bundle", zap.Int("key_amount", len(bundle.Keys)))
	return cbor.Marshal(bundle)
}

// GenerateSignedBundle creates a SignMessage containing the bundle, signed with signKey.
func GenerateSignedBundle(
	logger *zap.Logger,
	keySets []KeySet,
	signKey *ecdsa.PrivateKey,
	kid []byte,
) (signedBundle []byte, err error) {
	cborBundle, err := GenerateBundle(logger, keySets)
	if err != nil {
		logger.Error("error generating bundle", zap.Error(err))
		return nil, err
	}

	signMessage := cose.NewSignMessage(cborBundle)
	err = signMessage.SignWithESKey(signKey, kid)
	if err != nil {
		logger.Error("error signing bundle", zap.Error(err))
		return nil, err
	}

	return signMessage.MarshalCBOR()
}
