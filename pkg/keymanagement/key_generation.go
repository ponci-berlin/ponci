package keymanagement

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"go.uber.org/zap"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"
)

func RollKeys(
	logger *zap.Logger,
	writeDebugFiles bool,
	debugFilePath string,
	debugFilePrefix string,
) ([]byte, *ecdsa.PrivateKey, []byte, error) {
	newSignatureKey, err := generateSignatureKeys()
	if err != nil {
		logger.Error("error generating signing key", zap.Error(err))
		return nil, nil, nil, err
	}
	newEncryptionKey, err := generateEncryptionKey()
	if err != nil {
		logger.Error("error generating encryption key", zap.Error(err))
		return nil, nil, nil, err
	}

	publicKey := &newSignatureKey.PublicKey

	ecCose, err := cose.MarshallECKey(publicKey)
	if err != nil {
		logger.Error("error marshalling public key", zap.Error(err))
		return nil, nil, nil, err
	}

	if writeDebugFiles {
		coseKeyPath := path.Join(debugFilePath, debugFilePrefix+"ecdsa-cose.hex")
		err = WriteHexFile(ecCose, coseKeyPath)
		if err != nil {
			logger.Error(
				"error writing cose hex file",
				zap.Error(err),
				zap.Binary("content", ecCose),
				zap.String("path", coseKeyPath),
			)
			return nil, nil, nil, err
		}
		aesKeyPath := path.Join(debugFilePath, debugFilePrefix+"aes.hex")
		err = WriteHexFile(newEncryptionKey, aesKeyPath)
		if err != nil {
			logger.Error(
				"error writing AES hex file",
				zap.Error(err),
				zap.Binary("content", newEncryptionKey),
				zap.String("path", aesKeyPath),
			)
			return nil, nil, nil, err
		}
	}

	id := NewKeyID(publicKey)

	return id, newSignatureKey, newEncryptionKey, nil
}

func generateSignatureKeys() (*ecdsa.PrivateKey, error) {
	return ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
}

func generateEncryptionKey() ([]byte, error) {
	key := make([]byte, 16)
	var err error
	for i := 0; i < 5; i++ {
		_, err = rand.Read(key)
		if err == nil {
			break
		}
	}
	if err != nil {
		return nil, fmt.Errorf("reading random source: %w", err)
	}
	return key, nil
}

func WriteHexFile(key []byte, target string) error {
	keyString := hex.EncodeToString(key)
	return ioutil.WriteFile(target, []byte(keyString), os.ModePerm) // TODO handle err
}
