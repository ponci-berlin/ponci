/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keymanagement

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"fmt"
	"math/big"
	"time"

	"go.uber.org/zap"

	"github.com/fxamacker/cbor/v2"
	"gorm.io/gorm"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

type KeySet struct {
	CredType  credential.Type
	AESKey    []byte
	ECCKey    *ecdsa.PublicKey `gorm:"type:bytes"`
	OrgID     string
	ValidFrom time.Time `cbor:"-"`
	ValidTo   time.Time `cbor:"-"`
}

// KeyRecord represents a keyset in the database.
// This is only for use with GORM. Use the KeySet type instead
// TODO replace this with a better solution.
type KeyRecord struct {
	gorm.Model
	CredType  credential.Type
	AESKey    []byte
	ECCKey    []byte
	OrgID     string
	ValidFrom time.Time
	ValidTo   time.Time
}

func KeySetFromCBOR(marshalledKeySet []byte) (KeySet, error) {
	var cborKeySet cborKeySet
	err := cbor.Unmarshal(marshalledKeySet, &cborKeySet)
	if err != nil {
		return KeySet{}, err
	}
	return cborKeySet.ToKeySet(), nil
}

func (kr *KeyRecord) toKeySet() (KeySet, error) {
	if kr.AESKey == nil || kr.ECCKey == nil {
		return KeySet{}, fmt.Errorf("can't convert empty key record to keyset")
	}
	eccKey, err := cose.UnMarshallECKey(kr.ECCKey)
	if err != nil {
		return KeySet{}, err
	}
	return KeySet{
		CredType:  kr.CredType,
		AESKey:    kr.AESKey,
		ECCKey:    eccKey,
		OrgID:     kr.OrgID,
		ValidFrom: kr.ValidFrom,
		ValidTo:   kr.ValidTo,
	}, nil
}

func GetValidKeySets(logger *zap.Logger, dbHandle *gorm.DB) ([]KeySet, error) {
	var keyRecords []KeyRecord
	logger.Debug("getting all keysets that are valid", zap.Time("valid_to", time.Now().UTC()))
	tx := dbHandle.Where("valid_to >= ?", time.Now().UTC()).Find(&keyRecords)
	if tx.Error != nil {
		return nil, tx.Error
	}

	keySets := make([]KeySet, 0, len(keyRecords))
	for _, kr := range keyRecords {
		logger.Debug("serialising keyset", zap.Uint("db_id", kr.ID))
		keySet, err := kr.toKeySet()
		if err != nil {
			return nil, err
		}
		keySets = append(keySets, keySet)
	}
	return keySets, nil
}

func (ks *KeySet) Save(db *gorm.DB) error {
	keyBytes, err := cose.MarshallECKey(ks.ECCKey)
	if err != nil {
		return err // TODO: implement when not frustrated
	}
	kr := KeyRecord{
		CredType:  ks.CredType,
		AESKey:    ks.AESKey,
		ECCKey:    keyBytes,
		OrgID:     ks.OrgID,
		ValidFrom: ks.ValidFrom,
		ValidTo:   ks.ValidTo,
	}

	result := db.Save(&kr)
	return result.Error
}

func (KeyRecord) TableName() string {
	return "keys"
}

type cborKeySet struct {
	_        struct{} `cbor:",toarray"`
	CredType int
	Aeskey   []byte
	X        []byte
	Y        []byte
}

func (ks *KeySet) toCBORKeySet() cborKeySet {
	eckey := ks.ECCKey
	byteLen := (eckey.Curve.Params().BitSize + 7) / 8 //nolint:gomnd

	x, y := eckey.X, eckey.Y

	xBytes := make([]byte, byteLen)
	yBytes := make([]byte, byteLen)

	x.FillBytes(xBytes)
	y.FillBytes(yBytes)

	return cborKeySet{
		CredType: int(ks.CredType),
		Aeskey:   ks.AESKey,
		X:        xBytes,
		Y:        yBytes,
	}
}

func (cks *cborKeySet) ToKeySet() KeySet {
	eckey := &ecdsa.PublicKey{
		Curve: elliptic.P521(),
		X:     (&big.Int{}).SetBytes(cks.X),
		Y:     (&big.Int{}).SetBytes(cks.Y),
	}

	return KeySet{
		CredType: credential.Type(cks.CredType),
		AESKey:   cks.Aeskey,
		ECCKey:   eckey,
	}
}

func (ks *KeySet) MarshalCBOR() ([]byte, error) {
	return cbor.Marshal(ks.toCBORKeySet())
}
