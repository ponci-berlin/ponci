/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package authentication

import (
	"encoding/base32"
	"fmt"
	"time"

	"go.uber.org/zap"

	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"

	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
)

const (
	// seconds, 30 is the standard default.
	otpPeriod = 30
	// number of periods the two ends can be out by.
	otpSkew = 1
	// 8 is the longest supported by the library we use,
	// and is a sane value as a compromise between exactness
	// and information leakage.
	otpDigits = 8
)

var OTPOpts = totp.ValidateOpts{
	Period:    otpPeriod,
	Skew:      otpSkew,
	Digits:    otpDigits,
	Algorithm: otp.AlgorithmSHA256,
}

var b32 = base32.StdEncoding.WithPadding(base32.NoPadding)

func NewOTP() (string, string) {
	secret := keymanagement.NewRandomString() // strong randomness is desirable, but not strictly required
	secretString := b32.EncodeToString(secret)
	pass, _ := GenerateOTPPassword(secretString) // err can only ever be non-nil if we pass in something that isn't base32
	return secretString, pass
}

func GenerateOTPPassword(secret string) (string, error) {
	// err can only ever be non-nil if we pass in something that isn't base32
	pass, err := totp.GenerateCodeCustom(secret, time.Now(), OTPOpts)
	if err != nil {
		return "", fmt.Errorf("supplied password not valid: %w", err)
	}
	return pass, nil
}

func ValidateOTP(logger *zap.Logger, secret string, pass string) bool {
	result, err := totp.ValidateCustom(pass, secret, time.Now(), OTPOpts)
	if err != nil {
		logger.Error("couldn't validate OTP", zap.Error(err))
	}
	return err == nil && result
}
