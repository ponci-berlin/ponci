// +build !integration

/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package authentication_test

import (
	rand2 "math/rand"
	"testing"
	"time"

	"gitlab.com/ponci-berlin/ponci/pkg/logging"
	"go.uber.org/zap"

	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ponci-berlin/ponci/pkg/authentication"
	"golang.org/x/crypto/argon2"
)

//nolint:gochecknoinits
func init() {
	logger = logging.New()
}

var (
	logger *zap.Logger
)

func BenchmarkGenerateHash(b *testing.B) {
	b.ReportAllocs()
	password := keymanagement.NewRandomString()
	salt := keymanagement.NewRandomString()
	c := authentication.DefaultPasswordConfig
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		argon2.IDKey(password, salt, c.Time, c.Memory, c.Threads, c.KeyLen)
	}
}

func BenchmarkGenerationHashParallel(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i <= b.N; i++ {
		go func() {
			_, _ = authentication.GenerateHash(
				authentication.DefaultPasswordConfig,
				keymanagement.NewRandomString(),
				rand2.New(rand2.NewSource(time.Now().UnixNano())), //nolint:gosec
			)
		}()
	}
}

func TestGenerateAndCompareHash(t *testing.T) {
	// Try and force an OOM.
	// This is probably better suited for a Benchmark function, but I want this to fail the tests.
	password := keymanagement.NewRandomString()
	//nolint:gosec
	hash, _ := authentication.GenerateHash(
		authentication.DefaultPasswordConfig,
		password,
		rand2.New(rand2.NewSource(time.Now().UnixNano())),
	)
	err := authentication.CompareHashAndPassword(hash, password)
	assert.NoError(t, err)
}

func TestGenerateAndCompareHOTP(t *testing.T) {
	secret, pass := authentication.NewOTP()
	assert.True(t, authentication.ValidateOTP(logger, secret, pass))
}

func TestGenerateExistingKey(t *testing.T) {
	secret, _ := authentication.NewOTP()
	pass, err := authentication.GenerateOTPPassword(secret)
	assert.NoError(t, err)
	assert.True(t, authentication.ValidateOTP(logger, secret, pass))
}

func TestInvalidTOPT(t *testing.T) {
	secret, pass := authentication.NewOTP()
	fakePass := "12345678"
	if fakePass == pass {
		fakePass = "87654321"
	}
	assert.False(t, authentication.ValidateOTP(logger, secret, fakePass))
}
