// +build integration

/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package baercode_test

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"io/ioutil"
	"os"
	"path"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ponci-berlin/ponci/pkg/baercode"

	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

const (
	b64BaerCodeFileName = "php_baercode.b64"
	cryptKeyFileName    = "php_baercode_cryptkey.hex"
	pubKeyFileName      = "php_baercode_pubkey.pem"
	scratchDirKey       = "SCRATCH_DIR"
)

func TestVerifyBaerCodeIntegration(t *testing.T) {
	scratchDir := os.Getenv(scratchDirKey)
	if scratchDir == "" {
		t.Fatalf("not running without SCRATCH_DIR set")
	}

	procedures := []credential.Procedure{
		{
			Type: 1,
			Time: time.Date(2021, time.May, 1, 8, 0, 0, 0, time.UTC),
		},
	}

	expectedCredential := credential.Credential{
		FirstName:         "Max",
		LastName:          "Mustermann",
		DateOfBirth:       time.Date(1990, time.April, 1, 0, 0, 0, 0, time.UTC),
		Procedures:        procedures,
		ProcedureOperator: "PoNC GmbH",
		ProcedureResult:   false,
	}
	var expectedVersion uint16 = 1

	b64BaerCodePath := path.Join(scratchDir, b64BaerCodeFileName)
	cryptKeyPath := path.Join(scratchDir, cryptKeyFileName)
	pubKeyPath := path.Join(scratchDir, pubKeyFileName)

	b64BaerCode, err := ioutil.ReadFile(b64BaerCodePath)
	if err != nil {
		t.Fatalf("couldn't read baercode %s: %s", b64BaerCodePath, err)
	}

	hexKey, err := ioutil.ReadFile(cryptKeyPath)
	if err != nil {
		t.Fatalf("Couldn't read cryptkey %s: %s", cryptKeyPath, err)
	}

	aesKey := make([]byte, hex.DecodedLen(len(hexKey)))
	_, err = hex.Decode(aesKey, hexKey)
	if err != nil {
		t.Fatalf("couldn't hex decode key  %s: %s", hexKey, err)
	}

	pemKey, err := ioutil.ReadFile(pubKeyPath)
	if err != nil {
		t.Fatalf("couldn't read public key file %s: %s", pubKeyPath, err)
	}

	block, _ := pem.Decode(pemKey)
	pubKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		t.Fatalf("couldn't x509 decoded public key: %s", err)
	}

	cred, version, err := baercode.VerifyBaerCode(string(b64BaerCode), pubKey.(*ecdsa.PublicKey), aesKey)
	if err != nil {
		t.Fatalf("couldn't verify baercode: %s", err)
	}

	assert.Equal(t, expectedVersion, version)
	assert.Equal(t, expectedCredential.FirstName, cred.FirstName)
	assert.Equal(t, expectedCredential.LastName, cred.LastName)
	assert.Equal(t, expectedCredential.DateOfBirth.UTC(), cred.DateOfBirth.UTC())
	assert.Equal(t, expectedCredential.Procedures[0].Type, cred.Procedures[0].Type)
	assert.Equal(t, expectedCredential.Procedures[0].Time.UTC(), cred.Procedures[0].Time.UTC())
	assert.Equal(t, expectedCredential.ProcedureOperator, cred.ProcedureOperator)
	assert.Equal(t, expectedCredential.ProcedureResult, cred.ProcedureResult)
}
