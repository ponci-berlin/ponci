// +build integration

/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cose_test

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"os"
	"path"
	"testing"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"
)

const (
	signedMessageFile = "php_signmessage.cbor"
	pubKeyFile        = "php_publickey.pem"
	scratchDirKey     = "SCRATCH_DIR"
)

func TestVerifySignMessageIntegration(t *testing.T) {
	scratchDir := os.Getenv(scratchDirKey)
	if scratchDir == "" {
		t.Fatalf("not running without SCRATCH_DIR set")
	}

	signedMessagePath := path.Join(scratchDir, signedMessageFile)
	pubKeyPath := path.Join(scratchDir, pubKeyFile)

	hexMsg, err := os.ReadFile(signedMessagePath)
	if err != nil {
		t.Errorf("couldn't read encrypt0 message %s: %s", signedMessagePath, err)
	}

	pemKey, err := os.ReadFile(pubKeyPath)
	if err != nil {
		t.Errorf("couldn't read pubkey file %s: %s", pubKeyPath, err)
	}

	block, _ := pem.Decode(pemKey)

	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		t.Errorf("couldn't parse key: %s", err)
	}
	rawMsg := make([]byte, hex.DecodedLen(len(hexMsg)))
	_, err = hex.Decode(rawMsg, hexMsg)
	if err != nil {
		t.Errorf("couldn't hex decode encrypt0 message %s: %s", hexMsg, err)
	}

	msg, err := cose.SignMessageFromCBOR(rawMsg)
	if err != nil {
		t.Errorf("couldn't unmarshal raw msg: %s", err)
	}

	err = msg.VerifyWithESKey(pub.(*ecdsa.PublicKey))
	if err != nil {
		t.Errorf("couldn't verify message: %s", err)
	}
}
