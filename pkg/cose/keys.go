/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cose

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"errors"
	"fmt"
	"math/big"

	"github.com/PONCI-Berlin/go-cose"
	"github.com/fxamacker/cbor/v2"
)

const (
	KeyTypeECDSA     = cose.KeyTypeECDSA
	KeyTypeSymmetric = 4

	AlgorithmAESGCM128 = 1

	CurveNISTP521 = 3
)

var (
	ErrUnexpectedKeyType   = errors.New("unexpected key type")
	ErrUnexpectedAlgorithm = errors.New("unexpected algorithm")
)

type key struct {
	Kty    cose.KeyType `cbor:"1,keyasint"`
	Kid    []byte       `cbor:"2,keyasint,omitempty"`
	Alg    int          `cbor:"3,keyasint"`
	KeyOps []int        `cbor:"4,keyasint,omitempty"`
	BaseIV []byte       `cbor:"5,keyasint,omitempty"`
}

// key type 2.
type ecKey struct {
	D   []byte `cbor:"-4,keyasint,omitempty"`
	Y   []byte `cbor:"-3,keyasint"`
	X   []byte `cbor:"-2,keyasint"` // public key
	Crv int    `cbor:"-1,keyasint"` // 3 for NIST P-521
	key
}

func MarshallECKey(eckey *ecdsa.PublicKey) ([]byte, error) {
	byteLen := (eckey.Curve.Params().BitSize + 7) / 8 //nolint:gomnd

	x, y := eckey.X, eckey.Y

	xBytes := make([]byte, byteLen)
	yBytes := make([]byte, byteLen)

	x.FillBytes(xBytes)
	y.FillBytes(yBytes)

	obj := ecKey{
		D:   nil,
		Y:   yBytes,
		X:   xBytes,
		Crv: CurveNISTP521,
		key: key{
			Kty:    cose.KeyTypeECDSA,
			Kid:    nil,
			Alg:    cose.ES512.Value,
			KeyOps: nil,
			BaseIV: nil,
		},
	}
	marshalled, err := cbor.Marshal(obj)
	if err != nil {
		return nil, fmt.Errorf("mashalling cose key object to cbor bytes: %w", err)
	}
	return marshalled, nil
}

func UnMarshallECKey(data []byte) (*ecdsa.PublicKey, error) {
	coseKey := ecKey{}
	err := cbor.Unmarshal(data, &coseKey)
	if err != nil {
		return nil, fmt.Errorf("parsing cose ecdsa key: %w", err)
	}

	if coseKey.key.Kty != cose.KeyTypeECDSA {
		return nil, ErrUnexpectedKeyType
	}

	if coseKey.key.Alg != cose.ES512.Value {
		return nil, ErrUnexpectedAlgorithm
	}

	ecKey := &ecdsa.PublicKey{
		Curve: elliptic.P521(),
		X:     (&big.Int{}).SetBytes(coseKey.X),
		Y:     (&big.Int{}).SetBytes(coseKey.Y),
	}

	return ecKey, nil
}

type AESKey struct {
	K []byte `cbor:"-1,keyasint"`
	key
}

func MarshallAESKey(aeskey []byte) ([]byte, error) {
	obj := AESKey{
		K: aeskey,
		key: key{
			Kty:    KeyTypeSymmetric,
			Kid:    nil,
			Alg:    AlgorithmAESGCM128,
			KeyOps: nil,
			BaseIV: nil,
		},
	}
	marshalled, err := cbor.Marshal(obj)
	if err != nil {
		return nil, fmt.Errorf("mashalling cose key object to cbor bytes: %w", err)
	}
	return marshalled, nil
}

func UnMarshallAESKey(data []byte) ([]byte, error) {
	coseKey := AESKey{}
	err := cbor.Unmarshal(data, &coseKey)
	if err != nil {
		return nil, fmt.Errorf("parsing cose AES key: %w", err)
	}
	if coseKey.key.Kty != KeyTypeSymmetric {
		return nil, ErrUnexpectedKeyType
	}

	if coseKey.key.Alg != AlgorithmAESGCM128 {
		return nil, ErrUnexpectedAlgorithm
	}

	return coseKey.K, nil
}
