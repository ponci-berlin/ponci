/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cose_test

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"testing"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"

	"github.com/stretchr/testify/assert"
)

func TestMarshallECKey(t *testing.T) {
	privkey, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	assert.NoError(t, err)
	key := privkey.PublicKey
	buf, err := cose.MarshallECKey(&key)
	assert.NotNil(t, buf)
	assert.NoError(t, err)

	key2, err := cose.UnMarshallECKey(buf)
	assert.NoError(t, err)
	assert.True(t, key.Equal(key2))
}

func TestMarshallAESKey(t *testing.T) {
	buf := make([]byte, 16)
	_, err := rand.Read(buf)
	assert.NoError(t, err)
	key, err := cose.MarshallAESKey(buf)
	assert.NotNil(t, buf)
	assert.NoError(t, err)

	key2, err := cose.UnMarshallAESKey(key)
	assert.NoError(t, err)
	assert.Equal(t, buf, key2)
}
