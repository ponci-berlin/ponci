// +build integration

/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cose_test

import (
	"encoding/hex"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"
)

const (
	encryptFileName = "php_encrypt0.cbor"
	cryptKeyName    = "php_key.hex"
)

func TestDecodeEncrypt0MessageIntegration(t *testing.T) {
	scratchDir := os.Getenv(scratchDirKey)
	if scratchDir == "" {
		t.Fatalf("not running without SCRATCH_DIR set")
	}

	encPath := path.Join(scratchDir, encryptFileName)
	cryptKeyPath := path.Join(scratchDir, cryptKeyName)

	testPayload := []byte("test12356")

	hexMsg, err := os.ReadFile(encPath)
	if err != nil {
		t.Fatalf("couldn't read encrypt0 message %s: %s", encPath, err)
	}

	hexKey, err := os.ReadFile(cryptKeyPath)
	if err != nil {
		t.Fatalf("couldn't read encrypt0 message %s: %s", cryptKeyPath, err)
	}

	rawMsg := make([]byte, hex.DecodedLen(len(hexMsg)))
	_, err = hex.Decode(rawMsg, hexMsg)
	if err != nil {
		t.Fatalf("couldn't hex decode encrypt0 message %s: %s", hexMsg, err)
	}

	key := make([]byte, hex.DecodedLen(len(hexKey)))
	_, err = hex.Decode(key, hexKey)
	if err != nil {
		t.Fatalf("couldn't hex decode key  %s: %s", hexKey, err)
	}

	msg, err := cose.Encrypt0MessageFromCBOR(rawMsg)
	if err != nil {
		t.Fatalf("couldn't unmarshal raw msg: %s", err)
	}

	err = msg.DecodeAESGCMMessage(key)
	if err != nil {
		t.Fatalf("couldn't decrypt message: %s", err)
	}

	assert.Equal(t, testPayload, msg.Payload)
}
