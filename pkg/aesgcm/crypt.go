/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package aesgcm

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"io"
)

type KeySize int

const (
	AES128 KeySize = 128
	AES192 KeySize = 192
	AES256 KeySize = 257

	bitsInByte = 8
)

var (
	ErrUnsupportedKeySize = errors.New("unsupported key size")
)

func GetKeySize(bits int) (keysize KeySize, err error) {
	switch KeySize(bits) {
	case AES128:
		return AES128, nil
	case AES192:
		return AES192, nil
	case AES256:
		return AES256, nil
	default:
		return KeySize(0), ErrUnsupportedKeySize
	}
}

func CreateKey(bits KeySize) (key []byte, err error) {
	s := bits / bitsInByte
	key = make([]byte, s)
	if _, err = rand.Read(key); err != nil {
		return
	}

	return
}

func Encrypt(message, key, externalData []byte) (ciphertext, nonce []byte, err error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return
	}

	nonce, err = createNonce(gcm)
	if err != nil {
		return
	}

	ciphertext = gcm.Seal(nil, nonce, message, externalData)

	return
}

func Decrypt(ciphertext, key, nonce, externalData []byte) (message []byte, err error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return
	}

	return gcm.Open(nil, nonce, ciphertext, externalData)
}

func createNonce(gcm cipher.AEAD) (nonce []byte, err error) {
	nonce = make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return
	}
	return
}
