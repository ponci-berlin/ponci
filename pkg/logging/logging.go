// Package logging is a package to initialise and inject a zap logger into the web issuer.
// Everything here is pretty tightly coupled to the web issuer, so it'll be hard to reuse.
package logging

import (
	"fmt"
	"os"

	"go.uber.org/zap/zapcore"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

const (
	ComponentField    = "component"
	SubComponentField = "subcomponent"
	loggerKey         = "logger"
)

func baseEncoderConfig() zapcore.EncoderConfig {
	return zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		FunctionKey:    zapcore.OmitKey,
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.RFC3339TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
}

func baseConfig() zap.Config {
	return zap.Config{
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig:    baseEncoderConfig(),
	}
}

// localConfig is very much like the development config, and is meant specifically for local logging.
func localConfig() zap.Config {
	c := baseConfig()
	c.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	c.Development = true
	c.Encoding = "console"
	c.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return c
}

// devConfig is essentially the same as a `NewProduction` but with log level set to debug. This is meant for the dev
// environment, so that we can have debug levels parsed via JSON.
func devConfig() zap.Config {
	c := baseConfig()
	c.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	c.Development = true
	c.Encoding = "json"
	return c
}

// prodConfig is essentially the same as `NewProduction` except the timestamp is set to RFC3339.
func prodConfig() zap.Config {
	c := baseConfig()
	c.Level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	c.Development = false
	c.Encoding = "json"
	c.Sampling = &zap.SamplingConfig{
		Initial:    100, //nolint:gomnd
		Thereafter: 100, //nolint:gomnd
	}
	return c
}

// New creates a new logger based on the gin mode.
// Note, this panics if the logger can't be initialised.
func New() *zap.Logger {
	var logger *zap.Logger
	var err error
	if gin.IsDebugging() {
		if isContainer() {
			logger, err = devConfig().Build()
		} else {
			logger, err = localConfig().Build()
		}
	} else {
		logger, err = prodConfig().Build()
	}
	if err != nil {
		panic(fmt.Sprintf("Couldn't initialise logger: %s", err))
	}
	return logger
}

// Fetch fetches a new logger from the gin context, or if not found, initialises a new one and adds itself
// to the context and returns that.
func Fetch(ctx *gin.Context) *zap.Logger {
	_, ok := ctx.Get(loggerKey)
	if !ok {
		ctx.Set(loggerKey, New())
	}
	l, ok := ctx.Get(loggerKey)
	if !ok {
		panic("Couldn't fetch logger, even after setting it.")
	}

	return l.(*zap.Logger)
}

// Injector injects the logger in the gin context, to be used as middleware.
func Injector(logger *zap.Logger) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(loggerKey, logger)
		ctx.Next()
	}
}

// isContainer checks if we're running in our container setup, it does this naively by checking if the executable called
// was `/ponc`.
func isContainer() bool {
	exe, err := os.Executable()
	if err != nil {
		return false
	}
	return exe == "/ponc"
}
