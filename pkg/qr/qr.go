/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package qr

import (
	"bytes"
	"image/png"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
)

// Create takes a string and encodes them in a QR code.
// It then creates an image containing the QR code, scaled to a size x size pixel square.
func Create(content string, size int) (image []byte, err error) {
	code, err := qr.Encode(content, qr.L, qr.Unicode)
	if err != nil {
		return nil, err
	}

	code, err = barcode.Scale(code, size, size)
	if err != nil {
		return nil, err
	}

	var imageBuf bytes.Buffer
	err = png.Encode(&imageBuf, code)
	if err != nil {
		return nil, err
	}
	image = imageBuf.Bytes()
	return
}
