/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keyserver

import (
	"bytes"
	"context"
	"errors"
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"go.uber.org/zap"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

const (
	cborContentType = "application/cbor"
	certContentType = "application/x-x509-ca-cert"
)

var (
	bundlePublished = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "kms_bundle_published_total",
		Help: "The total number of times the bundle was published.",
	}, []string{
		"location",
	})
)

type uploadOpts struct {
	Endpoint        string
	BucketName      string
	AccessKeyID     string
	SecretAccessKey string
	UseSSL          bool
}

func publishBundle(logger *zap.Logger, ks *KeyServer) error {
	// Success gets set if any upload went ok.
	success := false

	signedBundle, err := ks.Keyman.GenerateSignedBundle(logger, ks.DB)
	if err != nil {
		logger.Error("failed to generate signing bundle", zap.Error(err))
		return fmt.Errorf("couldn't generate signing bundle: %w", err)
	}

	for _, location := range ks.PublishLocations {
		if location.Type != "s3" {
			logger.Error("endpoint not supported", zap.String("endpoint_type", location.Type))
			continue
		}

		// TODO: Maybe useful to check if the published bundle is the same?
		uOpts := uploadOpts{
			Endpoint:        location.EndPoint,
			BucketName:      location.BucketName,
			AccessKeyID:     location.AccessKeyID,
			SecretAccessKey: location.SecretAccessKey,
			UseSSL:          location.UseSSL,
		}

		bundlePublished.WithLabelValues(location.EndPoint).Inc()

		if err := s3Upload(logger, ks.CertName, ks.Keyman.GetRawCert(), certContentType, uOpts); err != nil {
			logger.Error("error publishing public key", zap.Error(err))
			continue
		}

		if err := s3Upload(logger, ks.CertBundleName, signedBundle, cborContentType, uOpts); err != nil {
			logger.Error("error publishing bundle", zap.Error(err))
			continue
		}

		success = true
	}

	if !success {
		return errors.New("could not publish bundle to any locations")
	}

	return nil
}

func s3Upload(logger *zap.Logger, objectName string, objectContent []byte, contentType string, opts uploadOpts) error {
	ctx := context.Background()
	logger = logger.With(
		zap.String("endpoint", opts.Endpoint),
		zap.String("bucket_name", opts.BucketName),
		zap.String("object_name", objectName),
	)
	logger.Info(
		"starting upload",
	)

	minioClient, err := minio.New(opts.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(opts.AccessKeyID, opts.SecretAccessKey, ""),
		Secure: opts.UseSSL,
	})
	if err != nil {
		logger.Error("couldn't initialise minio", zap.Error(err))
		return fmt.Errorf("couldn't initialise minio client: %w", err)
	}
	r := bytes.NewReader(objectContent)
	_, err = minioClient.PutObject(ctx, opts.BucketName, objectName, r, int64(len(objectContent)), minio.PutObjectOptions{
		ContentType: contentType,
		// This forces the object to be public.
		UserMetadata: map[string]string{"x-amz-acl": "public-read"},
		CacheControl: "no-store, max-age=0",
	})

	if err != nil {
		logger.Error("couldn't upload", zap.Error(err))
		return fmt.Errorf("couldn't upload s3://%s/%s/%s: %w", opts.Endpoint, opts.BucketName, objectName, err)
	}

	logger.Info("completed upload")
	return nil
}
