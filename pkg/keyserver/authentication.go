/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keyserver

import (
	"errors"
	"net/http"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"gitlab.com/ponci-berlin/ponci/pkg/logging"
	"go.uber.org/zap"

	"github.com/gin-gonic/gin"
	"gitlab.com/ponci-berlin/ponci/pkg/authentication"
	"gitlab.com/ponci-berlin/ponci/pkg/user"
	"gorm.io/gorm"
)

const (
	elementsInAuthHeader = 3
)

var (
	authFailures = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "kms_authentication_failures_total",
		Help: "The total number of authentication failures.",
	}, []string{
		"userID",
	})
)

func authenticateRequest(dbHandle *gorm.DB, ctx *gin.Context) (bool, *user.User) {
	logger := logging.Fetch(ctx).With(zap.String(logging.SubComponentField, "authenticateRequest"))
	authHeader := ctx.GetHeader("AUTHORIZATION")
	if authHeader == "" {
		logger.Info("no header received")
		authFailures.WithLabelValues("").Inc()
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return false, nil
	}
	logger.Debug("header received", zap.String("header", authHeader))

	parts := strings.Split(authHeader, ":")
	if len(parts) != elementsInAuthHeader {
		logger.Info("malformed header")
		authFailures.WithLabelValues("").Inc()
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return false, nil
	}

	logger = logger.With(zap.String("user", parts[0]))

	u, err := user.Fetch(dbHandle, parts[0])
	if err != nil {
		authFailures.WithLabelValues("").Inc()
		if errors.Is(err, gorm.ErrRecordNotFound) {
			logger.Info("user not found")
			ctx.AbortWithStatus(http.StatusUnauthorized)
		} else {
			logger.Error("database error", zap.Error(err))
			ctx.AbortWithStatus(http.StatusUnauthorized)
		}

		return false, nil
	}

	err = authentication.CompareHashAndPassword(u.Password, []byte(parts[1]))
	if err != nil {
		authFailures.WithLabelValues(u.UserID).Inc()
		logger.Error("failed to verify password", zap.Error(err))
		ctx.String(http.StatusUnauthorized, "incorrect username or password")
		return false, nil
	}

	if !authentication.ValidateOTP(logger, u.OTPSecret, parts[2]) {
		logger.Error("failed to verify OTP")
		authFailures.WithLabelValues(u.UserID).Inc()
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return false, nil
	}

	return true, u
}
