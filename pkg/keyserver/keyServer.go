/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keyserver

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"gitlab.com/ponci-berlin/ponci/pkg/logging"

	"go.uber.org/zap"

	"github.com/gin-gonic/gin"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"
	"gorm.io/gorm"
)

const (
	// The keyserver doesn't care about when a key is valid, but this can be given to the verifier as a hint.
	validityStart = 12 * time.Hour
	// Vaccine keys should be valid for two years, this should be revised in 2022 with existing records updated.
	vaccineValidity = 24 * time.Hour * 730
	// Test should be "valid" for 36 hours.
	debugValidity = 36 * time.Hour
	// 12-24 hour validity delay + 3 days of use + 3 days of validity of PCR tests +
	// half a day of margin for errors etc = 7.5 days.
	testValidity = 180 * time.Hour
)

var (
	keysSubmitted = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "kms_keys_submitted_total",
		Help: "The total number of keys submitted.",
	}, []string{
		"userID",
		"organisation",
		"credentialType",
	})
	kmsErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "kms_errors_total",
		Help: "The total number of KMS errors.",
	})
)

type KeyServer struct {
	Keyman *keymanagement.KeyManager
	Config
	DB         *gorm.DB
	updateChan chan bool
}

type PublishLocation struct {
	Type            string `mapstructure:"type"`
	EndPoint        string `mapstructure:"endPoint"`
	BucketName      string `mapstructure:"bucketName"`
	AccessKeyID     string `mapstructure:"accessKeyID"`
	SecretAccessKey string `mapstructure:"secretAccessKey"`
	UseSSL          bool   `mapstructure:"useSSL"`
}

type Config struct {
	CertBundleName     string            `mapstructure:"certBundleName"`
	PublishRefreshTime time.Duration     `mapstructure:"publishRefreshTime"`
	CertName           string            `mapstructure:"certName"`
	SigningCert        string            `mapstructure:"signingCert"`
	SigningKey         string            `mapstructure:"signingKey"`
	PublishLocations   []PublishLocation `mapstructure:"publishLocations"`
}

func New(logger *zap.Logger, config Config, dbHandle *gorm.DB) (*KeyServer, error) {
	updateChan := make(chan bool)
	keyman, err := keymanagement.New(
		logger.With(zap.String(logging.SubComponentField, "keymanagement")),
		config.SigningKey,
		config.SigningCert,
	)
	if err != nil {
		return nil, err
	}
	ks := &KeyServer{keyman, config, dbHandle, updateChan}
	go ks.publishMonitor(logger.With(zap.String(logging.SubComponentField, "publisher")), config.PublishRefreshTime)
	return ks, nil
}

func (ks *KeyServer) HandleSubmitCert(ctx *gin.Context) {
	// If this fails we have bigger problems.
	defer ctx.Request.Body.Close()

	logger := logging.Fetch(ctx).With(zap.String(logging.SubComponentField, "HandleSubmitCert"))

	logger.Debug("handling submission")
	// authenticate requester
	authenticated, user := authenticateRequest(ks.DB, ctx)
	if !authenticated {
		logger.Info("couldn't authenticate request")
		return
	}

	logger = logger.With(
		zap.String("user_id", user.UserID),
		zap.String("org_id", user.OrgID),
		zap.Int("prod_mask", user.ProcedureSet),
	)
	logger.Info("authenticated")

	reqBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if writeError(ctx, logger, err, http.StatusBadRequest, "Unable to read request body") {
		return
	}

	keySet, err := keymanagement.ParseSubmitKeysRequest(reqBytes, user.OrgID)
	if writeError(ctx, logger, err, http.StatusBadRequest, "Unable to parse request") {
		return
	}

	if !user.ProcedureTypeAllowed(keySet.CredType) {
		_ = writeError(
			ctx,
			logger,
			fmt.Errorf("user %s not allowed to create keyset of type %d", user.UserID, keySet.CredType),
			http.StatusUnauthorized,
			"Not allowed to create this type of keyset",
		)
		return
	}

	keySet.ValidFrom, keySet.ValidTo = getKeySetValidity(keySet.CredType)
	logger = logger.With(zap.Int("cred_type", int(keySet.CredType)))

	logger.Debug("saving keySet")
	err = keySet.Save(ks.DB)
	if writeError(ctx, logger, err, http.StatusInternalServerError, "internal server error") {
		return
	}

	keysSubmitted.WithLabelValues(user.UserID, user.OrgID, strconv.Itoa(int(keySet.CredType))).Inc()
	id := keymanagement.NewKeyID(keySet.ECCKey)

	logger = logger.With(zap.Binary("key_id", id))

	logger.Info("keySet saved")

	ctx.Data(http.StatusOK, "application/octet-string", id)

	ks.updateChan <- true
}

// ServiceLiveness is an endpoint to see if the service is alive.
// Just returns a 200 for now, but once we have a lightweight way of checking if the service is
// working properly, we should add it here.
func (ks *KeyServer) ServiceLiveness(ctx *gin.Context) {
	defer ctx.Request.Body.Close()
	ctx.String(http.StatusOK, "OK")
}

// PublishBundle starts the publish process.
func (ks *KeyServer) PublishBundle() {
	ks.updateChan <- true
}

func (ks *KeyServer) publishMonitor(logger *zap.Logger, refreshTime time.Duration) {
	logger.Info("starting refresh timer", zap.Duration("refresh_time", refreshTime))
	refresh := time.NewTimer(refreshTime)
	logger.Info("monitoring update channel")
	// TODO: Add some logic here to make sure we don't update too often.
	for {
		select {
		case <-ks.updateChan:
			if err := publishBundle(logger, ks); err != nil {
				logger.Error("failed to publish", zap.Error(err))
			}
			refresh.Stop()
			refresh.Reset(refreshTime)
		// AFAIK you can't use two channels in a case statement.
		case <-refresh.C:
			logger.Info("starting period refresh")
			if err := publishBundle(logger, ks); err != nil {
				logger.Error("failed to publish", zap.Error(err))
			}
			refresh.Stop()
			refresh.Reset(refreshTime)
		}
	}
}

func getKeySetValidity(credType credential.Type) (validFrom time.Time, validTo time.Time) {
	now := time.Now().UTC()
	validFrom = now.Add(validityStart)
	switch credType {
	case credential.CredTypeNegativeTest:
		validTo = now.Add(testValidity)
	case credential.CredTypeVaccine:
		validTo = now.Add(vaccineValidity)
	case credential.CredTypeUnspecified:
		validTo = now.Add(debugValidity)
	case credential.CredTypeInvalid:
		validFrom = now
		validTo = now
	default:
		validFrom = now
		validTo = now
	}

	return validFrom, validTo
}

func writeError(ctx *gin.Context, logger *zap.Logger, err error, status int, message string) bool {
	if err == nil {
		return false
	}
	kmsErrors.Inc()
	logger.Error(message, zap.Error(err))
	ctx.String(status, message)
	return true
}
