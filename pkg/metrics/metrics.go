package metrics

import (
	"net/http"

	"go.uber.org/zap"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Config struct {
	ListenSocket string `mapstructure:"listenSocket"`
	MetricsPath  string `mapstructure:"metricsPath"`
}

func Start(logger *zap.Logger, config Config) {
	listenAddr := "0.0.0.0:11232"
	if config.ListenSocket != "" {
		listenAddr = config.ListenSocket
	}
	metricsPath := "/metrics"
	if config.MetricsPath != "" {
		metricsPath = config.MetricsPath
	}

	go func() {
		mux := http.NewServeMux()
		mux.Handle(metricsPath, promhttp.Handler())
		s := &http.Server{
			Addr:    listenAddr,
			Handler: mux,
		}
		logger.Info("finished", zap.Error(s.ListenAndServe()))
	}()
}
