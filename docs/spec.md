# BärCODE Spec

This is a description of the BärCODE data formats and how to use them. We will provide reference implementations of all BärCODE components in Golang (KMS, Verifier, Issuer), JavaScript (Issuer, Verifier), PHP (Issuer in the form of an integration lib), Python (command line verifier). 

## Anatomy of a BärCODE

The BärCODE is a COSE signed CBOR object bearing the proof-of-no-covid; i.e. a negative test result or a vaccination confirmation.

BärCODE includes the following fields:

  1. Last Name
  2. First Name
  3. Date of Birth
  4. Type of Procedure
  5. Time of Procedure
  6. Name of Testing Station / Vaccination Site
  7. Statement that test is negative / vaccination is complete

The data is encrypted, but the encryption key is included in the QR code. This is solely (weak) protection against shoulder surfing. For vaccinations, type and time of procedure are included multiple times.


COSE signed CBOR object ([RFC8152](https://www.rfc-editor.org/info/rfc8152), [RFC8949](https://www.rfc-editor.org/info/rfc8949)).

![BärCODE structure diagram](diagrams/images/baercode.png)

## Anatomy of a Key Bundle

The Key Bundle contains the public keys by all signers of BärCODEs, this is in turn signed with the BärCODE master key.

A Key Bundle is also a COSE signed CBOR object (CBOR Tag 98).

The Key Bundle's payload is a Bundle with the following contents:

1. A version 
2. A date (date the bundle was generated)
2. A list of keys

Each key is represented as a keyset which describes

1. The type of key it is (for test, for vaccination, for any).
2. The AES key 
3. The coordinates on the AES Key to derive the ECDSA public key. 

```plantuml
@startuml
'https://plantuml.com/object-diagram

map KeySet {
    CredType => int
    Aeskey => AES key in byte array
    X => X coordinate of ECDSA public key
    Y => Y coordinate of ECDSA public key
}

map Keys {
    0 *---> KeySet
    1 *---> KeySet
    ... *---> KeySet
}

map Bundle {
    Version => Bundle version
    Date => UNIX timestamp
    Keys *----> Keys
}

map "COSE_Signature Protected Headers" as sigprot {
    alg => ES512
}

map "COSE_Signature Unprotected Headers" as sigunprot {
    kid => PoNC key ID
}

map COSE_Signature {
    Protected Headers *---> sigprot
    Unprotected Headers *---> sigunprot
    signature => Cryptographic signature
}

map COSE_Signatures {
    PoNC signature *---> COSE_Signature
}

map SignedBundle {
    CBOR Tag => 98
    Protected Headers => empty
    Unprotected Headers => empty
    Payload *---> Bundle
    Signatures *---> COSE_Signatures
}

@enduml
```
The Key Bundle will be served as a static asset from S3. As it only holds publically available data, this is noncritical.

# Key Generation and Rotation.

Key Generation is handled by the verifier on site where the certificates are generated. See _BärCODE Issuer Flow_ below for usage.

We use Public/Private Key pairs for signing the BärCODEs. Keys are rotated daily for signing test proofs, and every 90 days for vaccination proofs. Public Keys will be added to the bundle that's made available to the verifier and kept in there for up to 3 days for test proof keys, and up to 380 days for vaccination proofs.

According to the permitted use determined during onboarding, signing keys will be valid either

  * for tests only (i.e. for testing sites)
  * for vaccination only (i.e. for vaccination centres)
  * or for both tests and vaccinations (i.e. for clinics and enterprise medical services) 
  
Key attributes will be checked for proper use during submission to us.

## Key revocation

If a need to revoke a given key arises, we will republish a new bundle with the key removed. That way we don't need to add key revocation lists. Keys for testing are short lived and we expect them to age out quickly enough that revocation won't be a common occurrence for them. Vaccination Proof Keys are high value sensitive assets and need to be protected and treated as such. 

# Verification

The [reference verifier app](https://verifier.bärcode.de) verifies BärCODEs and shows either a green confirmation screen with name and date-of-birth for cross check with an ID, or a red error screen with possible errors:

  * Test too old: If the test procedure time is older than the allowed maximum age
  * Test from the future: If the test procedure time is in the future
  * Invalid signature: If the content of the BärCODE doesn't match the signature
  * Unknown key: If the BärCODE is signed by a key that's not in the bundle

```plantuml
@startuml
start

title App verification flow.

if (Key Bundle older than X) then (yes)
  if (Internet available) then (yes)
    :Update Key Bundle;
  endif
endif

repeat
  :Read QR code;
  :Decompress QR code;
  :Decode QR code;
  :Verify signature;
  :Verify contents;
  if (all verifications valid?) then (yes)
    #green:Display approval screen with Name;
  else (no)
    #red:Display denial screen;
  endif
repeat while (app open?) is (yes)
-> no;
stop
@enduml

```

## The PONCi Services

The Proof-of-No-Covid Infrastructure services are the Key Management Server, the Verifier, and the Key Bundle service

  * Key Management Server: Used by registered test stations and vaccination centres to submit their public signing key to. Requires authentication.
  * Verifier: Offline capable web-app to verify BärCODEs. Used by anyone to verify a BärCODE. Publically accessible.
  * Key Bundle Service: Creates Key Bundles and puts them into S3 for perusal by Verifier(s). 


