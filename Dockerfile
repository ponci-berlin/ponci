FROM docker.io/library/golang:1.16 as gobuilder

WORKDIR /workspace

COPY go.mod go.mod
COPY go.sum go.sum

RUN go mod download

COPY cmd/ cmd/
COPY pkg/ pkg/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -ldflags="-extldflags=-static"  -a -o ponc ./cmd/

FROM scratch

WORKDIR /

COPY --from=gobuilder /workspace/ponc /ponc
COPY --from=gobuilder /etc/ssl/certs /etc/ssl/certs
COPY --from=gobuilder /usr/share/ca-certificates /usr/share/ca-certificates
COPY public/ public/

ENTRYPOINT ["/ponc"]
